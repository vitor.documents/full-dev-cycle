<?php

/**
 * @see       https://github.com/laminas-api-tools/api-tools-skeleton for the canonical source repository
 * @copyright https://github.com/laminas-api-tools/api-tools-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas-api-tools/api-tools-skeleton/blob/master/LICENSE.md New BSD License
 */

/**
 * List of enabled modules for this application.
 */
return array(
    'Application',
    'Laminas\DevelopmentMode',
    'Laminas\ApiTools',
    'Laminas\ApiTools\Provider',
    'Laminas\ApiTools\Documentation',
    'AssetManager',
    'Laminas\ApiTools\ApiProblem',
    'Laminas\ApiTools\Configuration',
    'Laminas\ApiTools\OAuth2',
    'Laminas\ApiTools\MvcAuth',
    'Laminas\ApiTools\Hal',
    'Laminas\ApiTools\ContentNegotiation',
    'Laminas\ApiTools\ContentValidation',
    'Laminas\ApiTools\Rest',
    'Laminas\ApiTools\Rpc',
    'Laminas\ApiTools\Versioning',
);
