<?php

/**
 * This file was autogenerated by laminas-api-tools/api-tools-mvc-auth (bin/api-tools-mvc-auth-oauth2-override.php),
 * and overrides the Laminas\ApiTools\OAuth2\Service\OAuth2Server to provide the ability to create named
 * OAuth2\Server instances.
 */
return array(
    'service_manager' => array(
        'factories' => array(
            'Laminas\ApiTools\OAuth2\Service\OAuth2Server' => 'Laminas\ApiTools\MvcAuth\Factory\NamedOAuth2ServerFactory',
        ),
    ),
);